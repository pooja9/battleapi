'use strict';

const express = require('express');
const battlemodel = require('../dbSchema/battle_model');
const debug = require('debug')('battleapi:count');

const router = express.Router();

router.get('/', (req, res, next) => {
  debug('MW1: Count of battles');
  battlemodel.count().then(result => {
      debug('count of battles',result);
      res.status(200).send(result);
  }).catch(err => {
      debug('error while counting battles',err);
  })
});

module.exports = router;
