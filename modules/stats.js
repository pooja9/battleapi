'use strict';

const express = require('express');
const battlemodel = require('../dbSchema/battle_model');
const debug = require('debug')('battleapi:stats');

const router = express.Router();
let payload;

router.get('/', (req, res, next) => {
  debug('MW1: total number of attackers win');
  battlemodel.find({'attacker_outcome' : 'win'}).count().then(result => {
      debug('number of winning battles',result);
      res.locals.attackerwin = result;
      return next();
  }).catch(err => {
      debug('error while counting winning battles',err);
  })
});

router.get('/', (req, res, next) => {
  debug('MW2: total number of attackers loss');
  battlemodel.find({'attacker_outcome' : 'loss'}).count().then(result => {
      debug('number of losing battles',result);
      res.locals.attackerlose = result;
      return next();
  }).catch(err => {
      debug('error while counting losing battles',err);
  })
});

router.get('/', (req, res, next) => {
  debug('MW3: list of battle types');
  battlemodel.distinct('battle_type').then(result => {
      debug('list of battle types',result);
      res.locals.battletypes = result;
      return next();
  }).catch(err => {
      debug('error while listing battle types',err);
  })
});

router.get('/', (req, res, next) => {
  debug('MW4: defender size');
  battlemodel.aggregate([ 
    { "$group": { 
        "_id": null,
        "average": { "$avg": "$defender_size" },
        "min": { "$min": "$defender_size" }, 
        "max": { "$max": "$defender_size" }
    }}
  ]).then(result => {
    debug('defender size',result);
    res.locals.defendersize = result;
    return next();
  }).catch(err => {
    debug('error while checking defender size',err);
  })
});

router.get('/', (req, res, next) => {
  debug('MW5: most active attacker_king'); 
  battlemodel.aggregate([ 
        { "$unwind": "$attacker_king" },
        {
            "$group": {
                "_id": null,
                "count": { "$sum": 1 }
            }
        },
        { "$sort": { "count": -1 } },
        { "$limit": 1 }
  ]).then(result => {
    debug('attackerking',result);
    res.locals.attacker_king = result;
    return next();
  }).catch(err => {
    debug('error while checking attacker king',err);
  })
});

router.get('/', (req, res, next) => {
  debug('MW6: most active defender_king'); 
  battlemodel.aggregate([ 
        { "$unwind": "$defender_king" },
        {
            "$group": {
                "_id": null,
                "count": { "$sum": 1 }
            }
        },
        { "$sort": { "count": -1 } },
        { "$limit": 1 }
  ]).then(result => {
    debug('defender king',result);
    res.locals.attacker_king = result;
    return next();
  }).catch(err => {
    debug('error while checking defender king',err);
  })
});
  
router.get('/', (req, res, next) => {
  debug('MW7: most active region'); 
  battlemodel.aggregate([ 
        { "$unwind": "$region" },
        {
            "$group": {
                "_id": null,
                "count": { "$sum": 1 }
            }
        },
        { "$sort": { "count": -1 } },
        { "$limit": 1 }
    ]).then(result => {
      debug('region',result);
      res.locals.region = result;
      return next();
  }).catch(err => {
      debug('error while checking region',err);
  })
});

router.get('/', (req, res, next) => {
  debug('MW8: most active battle'); 
  battlemodel.aggregate([ 
        { "$unwind": "$name" },
        {
            "$group": {
                "_id": null,
                "count": { "$sum": 1 }
            }
        },
        { "$sort": { "count": -1 } },
        { "$limit": 1 }
  ]).then(result => {
      debug('name',result);
      res.locals.name = result;
      return next();
  }).catch(err => {
      debug('error while checking name of battle',err);
  })
});

router.get('/', (req, res, next) => {
  debug('MW9: sending response');
  payload = {
        'most_active': {
            'attacker_king': res.locals.attacker_king,
            'defender_king':res.locals.defender_king,
            'region': res.locals.region,
            'name': res.locals.name,
        },
        'attacker_outcome': {
            'win': res.locals.attackerwin,
            'loss': res.locals.attackerlose,
        },
        'battle_types': res.locals.battletypes,
        'defender_size':{
            'average':res.locals.defendersize.average,
            'min': res.locals.defendersize.min,
            'max': res.locals.defendersize.max
        }
    }
  res.status(200).send(payload);
});

module.exports = router;
