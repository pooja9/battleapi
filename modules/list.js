'use strict';

const express = require('express');
const battlemodel = require('../dbSchema/battle_model');
const debug = require('debug')('battleapi:list');

const router = express.Router();

router.get('/', (req, res, next) => {
  debug('MW1: List of battle places');
  battlemodel.distinct('location').then(result => {
      debug('Location of battles',result);
      res.status(200).send(result);
  }).catch(err => {
      debug('error while listing places',err);
  })
});

module.exports = router;
