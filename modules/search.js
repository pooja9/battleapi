'use strict';

const express = require('express');
const battlemodel = require('../dbSchema/battle_model');
const debug = require('debug')('battleapi:search');

const router = express.Router();
let element;

router.get('/', (req, res, next) => {
  debug('MW: search');
  element = JSON.stringify(req.query);
  battlemodel.find(element).then(result => {
    debug('search results',result);
    res.status(200).send(result);
  }).catch(err => {
    debug('error while listing battle types',err);
  })
});

module.exports = router;
