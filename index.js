'use strict';

const express = require('express');
const debug = require('debug')('battleapi');
const mongoose = require('mongoose');
const helmet = require('helmet');
const cors = require('cors');
const config = require('./config');
const bodyParser = require('body-parser');

const app = express();
const mongoDB = 'mongodb://pooja9:pooja9@ds163182.mlab.com:63182/battle_db';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(helmet());
app.use(cors());

//E1-list of all places where battle has taken place
app.use('/list', require('./modules/list'));

//E2-count of places
app.use('/count', require('./modules/count'));

//E3-statistics
app.use('/stats', require('./modules/stats'));

//E4-search
app.use('/search', require('./modules/search'));

app.listen(config.PORT, () => {
    debug(`Server started on - ${config.PORT}`);
});

module.exports = app;